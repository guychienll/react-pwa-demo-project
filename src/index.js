import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

import * as firebase from "firebase/app";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBziBz2KNWVb22cQidZ-MxA0zzKvTqfwK8",
  authDomain: "react-pwa-demo-project.firebaseapp.com",
  databaseURL: "https://react-pwa-demo-project.firebaseio.com",
  projectId: "react-pwa-demo-project",
  storageBucket: "react-pwa-demo-project.appspot.com",
  messagingSenderId: "328919020850",
  appId: "1:328919020850:web:929a059f1249ecb34bd32b",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
